variable "name" {
  type = string
}
variable "image_tag_mutability" {
  type = string
}
variable "tags" {
  type    = map(any)
  default = {}
}
variable "encryption_configuration" {
  type    = map(any)
  default = {}
}
variable "image_scanning_configuration" {
  type    = map(any)
  default = {}
}